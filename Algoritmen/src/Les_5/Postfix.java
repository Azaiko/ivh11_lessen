package Les_5;

import java.util.Stack;

/**
 * Created by Rodi on 14-3-2017.
 */
public class Postfix {

    public static String berekenPostfix(String s){
        Stack<Character> stack = new Stack<Character>();

        char plus = '+';
        char minus = '-';
        char multiply = '*';
        char divide = '/';

        int operators = 1;
        int numbers = 0;

        String sum;

        for (int i = 0; i < s.length(); i++){
            if (stack.isEmpty()){
                stack.push(s.charAt(0));
                numbers++;
            } else if (stack.peek() != plus &&
                    stack.peek() != minus &&
                    stack.peek() != multiply &&
                    stack.peek() != divide){
                stack.push(s.charAt(s.length() - operators));
                operators++;
            } else {
                stack.push(s.charAt(numbers));
                numbers++;
            }
        }

        sum = stack.toString();

        return sum;
    }
}
