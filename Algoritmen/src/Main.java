import Les_1.MagicSquare;
import Les_2.Fibonacci;
import Les_2.Hanoi;
import Les_2.Palindroom;
import Les_2.RecursiveBinarySearch;
import Les_3.MergeSortedArray_1;
import Les_3.MergeSortedArray_2;
import Les_4.GeneriekeMethode;
import Les_5.Haakjes;
import Les_5.Postfix;
import Stamboom.FamilyTree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Rodi on 2-2-2017.
 */

public class Main {

    public static void main(String[] args) throws IOException {
        //GET CONSOLE INPUT
        /*System.out.print("Enter something:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = br.readLine();
        int intInput = Integer.parseInt(input);*/

        FamilyTree familyTree = new FamilyTree();
        System.out.println(familyTree.getNodes().getChildren().get(1).getChildren().get(1));
    }

    public void les_5(){
        // HOOFDSTUK 5
        /* 5.10.3 Haakjes (Brackets) */
        //System.out.println(Haakjes.inBalans("{()(){}}"));

        /* 5.10.4 Postfix (Niet helemaal volgens opdracht)*/
        //System.out.println(Postfix.berekenPostfix("321+-"));
    }

    public void les_4(){
        // HOOFDSTUK 4
        /* Sheetopdracht */
        /*Number[][] allAnswers = {
                {9,8,6,7},
                {5,9,2,1},
                {63,1,3,7},
                {5,8,9,7}
        };
        GeneriekeMethode.maxRowSum(allAnswers);*/
    }

    public void les_3(){
        //  HOOfDSTUK 3
        /*  3.15.1 Merge two sorted arrays (Same as 1.8.7) */
        /* MergeSortedArray_1.mergeSortedRows(); */

        /*  3.15.2 Merge two arrayLists instead of int[] arrays*/
        /* MergeSortedArray_2.mergeSortedRows(); */
    }

    public void les_2(){
        //  HOOFDSTUK 2
        /*  2.10.4 Fibonacci recursion vs non recursion*/
        /*Fibonacci.fibo(1,1);
        Fibonacci.noRecursion();*/

        /*  2.10.5 Palindroom string check*/
        /*Boolean bool = Palindroom.checkPalindroom(input);
        if (bool)
            System.out.println("The string '" + input + "' is a palindroom.");
        else
            System.out.println("The string '" + input + "' is NOT a palindroom.");*/

        /*  2.10.6 Recursive Binary Search*
        /*        int[] intList = new int[1000];
        for (int a = 0; a < intList.length; a++)
            intList[a] = a;
        RecursiveBinarySearch.recursiveBinarySearch(250, 1, intList.length, intList, 0);
         */

        /*  2.10.7 Towers of Hanoi*/
        /* Hanoi.buildTowers(intInput); */
    }

    public void les_1(){
        //  HOOFDSTUK 1
        /*  1.8.1 Les_1.BucketSort (Maxval, int list)*/
        /*int[] data = {5, 3, 0, 2, 4, 1, 0, 5, 2, 3, 1, 4};
        Les_1.BucketSort.bucketSort(5,data);*/

        /*  1.8.3 Binary Search*/
        /*int[] intList = new int[1000];
        for (int a = 0; a < intList.length; a++)
            intList[a] = a;
        Les_1.BinarySearch.binarySearch(500, 1, intList.length, intList);*/

        /*  1.8.5 Linear Search*/
        //Les_1.LinearSearch.calculate();

        /*  1.8.7 Merge two sorted rows*/
        //Les_1.MergeSortedRows.mergeSortedRows();

        /*  1.8.8 Magic Square*/
        //MagicSquare.createThreeByThreeSquare();
    }

}

