package Les_1;

import java.util.Arrays;

/**
 * Created by Rodi on 9-2-2017.
 */
public class BucketSort {

    public static void bucketSort(int maxVal, int[] a) {

        System.out.println("Before: " + Arrays.toString(a));

        int[] bucket = new int[maxVal + 1];

        for (int i = 0; i < bucket.length; i++) {
            bucket[i] = 0;
        }

        for (int i = 0; i < a.length; i++) {
            bucket[a[i]]++;
        }

        int outPos = 0;
        for (int i = 0; i < bucket.length; i++) {
            for (int j = 0; j < bucket[i]; j++) {
                a[outPos++] = i;
            }
        }

        System.out.println("After:  " + Arrays.toString(a));
    }
}
