package Les_1;

/**
 * Created by Rodi on 9-2-2017.
 */
public class MagicSquare {
    public static void createThreeByThreeSquare(){
        int[][] magic = new int[3][3];
        int n = 3;
        int number = 1;

        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++) {
                magic[j][i] = number;
                number = number + 1;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(magic[i][j] + " ");
            }
            System.out.println();
        }
    }
}
