package Les_1;

import java.util.Date;

/**
 * Created by Rodi on 9-2-2017.
 */
public class LinearSearch {

    public static void calculate(){

        //a
        long totalTimePassedA = 0;
        for (int k = 0; k < 100; k++) {
            long t01 = new Date().getTime();
            long som = 0;
            int n = 10000000;
            for (int i = 0; i < n; i++)
                som++;
            long t02 = new Date().getTime();
            long timePassed = t02 - t01;
            totalTimePassedA = totalTimePassedA + timePassed;
        }
        long totalTimeA = totalTimePassedA / 100;
        System.out.println("Avarage time passed in example A is: " + totalTimeA + "ms");

        //b
        long totalTimePassedB = 0;
        for (int k = 0; k < 100; k++) {
            long t01 = new Date().getTime();
            long som = 0;
            int n = 10000;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    som++;
            long t02 = new Date().getTime();
            long timePassed = t02 - t01;
            totalTimePassedB = totalTimePassedB + timePassed;
        }
        long totalTimeB = totalTimePassedB / 100;
        System.out.println("Average time passed in example B is: " + totalTimeB + "ms");

        //c
        long totalTimePassedC = 0;
        for (int k = 0; k < 1; k++) {
            long t01 = new Date().getTime();
            long som = 0;
            int n = 1000;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n * n; j++)
                    som++;
            long t02 = new Date().getTime();
            long timePassed = t02 - t01;
            totalTimePassedC = totalTimePassedC + timePassed;
        }
        long totalTimeC = totalTimePassedC / 1;
        System.out.println("Average time passed in example C is: " + totalTimeC + "ms");

        //d
        long totalTimePassedD = 0;
        for (int k = 0; k < 100; k++) {
            long t01 = new Date().getTime();
            long som = 0;
            int n = 10000;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < i; j++)
                    som++;
            long t02 = new Date().getTime();
            long timePassed = t02 - t01;
            totalTimePassedD= totalTimePassedD + timePassed;
        }
        long totalTimeD = totalTimePassedD / 100;
        System.out.println("Average time passed in example D is: " + totalTimeD + "ms");
    }

}
