package Les_1;

import java.util.Arrays;

/**
 * Created by Rodi on 9-2-2017.
 */
public class BinarySearch {

    public static void binarySearch(int answer, int lowerbound, int upperbound, int[] intList) {
        int position;
        int comparisonCount = 1;
        position = (lowerbound + upperbound) / 2;

        while ((intList[position] != answer) && (lowerbound <= upperbound)) {
            comparisonCount++;
            if (intList[position] > answer) {
                upperbound = position - 1;
            } else {
                lowerbound = position + 1;
            }
            position = (lowerbound + upperbound) / 2;
        }
        if (lowerbound <= upperbound) {
            System.out.println("The number that was found is: " + position);
            System.out.println("The binary search found the number after " + comparisonCount + " comparisons.");
        } else
            System.out.println("Sorry, the number is not in this array.  The binary search made "
                    + comparisonCount + " comparisons.");
    }
}
