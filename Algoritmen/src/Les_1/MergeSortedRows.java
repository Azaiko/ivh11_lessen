package Les_1;

import java.util.Arrays;

/**
 * Created by Rodi on 9-2-2017.
 */
public class MergeSortedRows {
     public static void mergeSortedRows() {
        Integer[] arrayOne = {0,0,0,1,1,2,3,3,4,5,5,5};
        Integer[] arrayTwo = {0,1,2,2,2,2,2,3,3,3,3,4};
        System.out.println("Array one:  " + Arrays.toString(arrayOne));
        System.out.println("Array two:  " + Arrays.toString(arrayTwo));


        int[] merged = merge(arrayOne,arrayTwo);

        System.out.println("After:  " + Arrays.toString(merged));
    }

    public static int[] merge(Integer[] a, Integer[] b) {

        int[] merged = new int[a.length + b.length];
        int i = 0, j = 0, k = 0;

        while (i < a.length && j < b.length)
            merged[k++] = a[i] < b[j] ? a[i++] :  b[j++];

        while (i < a.length)
            merged[k++] = a[i++];

        while (j < b.length)
            merged[k++] = b[j++];

        return merged;
    }
}
