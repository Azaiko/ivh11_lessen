package Les_3;

import java.util.Arrays;

/**
 * Created by Rodi on 20-2-2017.
 */
public class MergeSortedArray_1 {
    public static void mergeSortedRows() {
        Integer[] arrayOne = {1,2,3,7,80,701,871,1054,5014,10414,15014,201141};
        Integer[] arrayTwo = {9,80,87,93,177,501,757,3414,8901,10407,10781,150407};
        System.out.println("Array one:  " + Arrays.toString(arrayOne));
        System.out.println("Array two:  " + Arrays.toString(arrayTwo));

        int[] merged = merge(arrayOne,arrayTwo);

        System.out.println("After:  " + Arrays.toString(merged));
    }

        private static int[] merge(Integer[] arrayOne, Integer[] arrayTwo) {
            int[] merged = new int[arrayOne.length + arrayTwo.length];

            int i = 0;
            int j = 0;
            int k = 0;

            while(i < arrayOne.length && j < arrayTwo.length){
                if (arrayOne[i] < arrayTwo[j])
                    merged[k++] = arrayOne[i++];
                else
                    merged[k++] = arrayTwo[j++];
            }

            while (i < arrayOne.length)
                merged[k++] = arrayOne[i++];

            while (j < arrayTwo.length)
                merged[k++] = arrayTwo[j++];


            return merged;
        }
}