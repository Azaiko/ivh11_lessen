package Les_3.Polynoom;

/**
 * Created by Rodi on 20-2-2017.
 */
public class Term {
    private int coëfficiïent;
    private int exponent;

    public Term(int coëfficiënt, int exponent){
        this.coëfficiïent = coëfficiënt;
        this.exponent = exponent;
    }

    public int getCoef(){
        return coëfficiïent;
    }

    public int getExp(){
        return exponent;
    }

    public String toString(){
        return coëfficiïent + "x^" + exponent;
    }
}
