package Les_3;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Rodi on 20-2-2017.
 */
public class MergeSortedArray_2 {

    public static void mergeSortedRows() {
        //ArrayList 1
        ArrayList<Integer> arrayOne = new ArrayList<>();
        Integer[] numbersOne = {1,2,3,7,80,701,871,1054,5014,10414,15014,201141};
        arrayOne.addAll(Arrays.asList(numbersOne));

        //ArrayList 2
        ArrayList<Integer> arrayTwo = new ArrayList<>();
        Integer[] numbersTwo = {9,80,87,93,177,501,757,876,1053,1055,20141,1047147};
        arrayTwo.addAll(Arrays.asList(numbersTwo));

        System.out.println("Array one:  " + arrayOne.toString());
        System.out.println("Array two:  " + arrayTwo.toString());

        ArrayList<Integer> merged = merge(arrayOne,arrayTwo);

        System.out.println("After:  " + merged.toString());
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {

        ArrayList<Integer> merged = new ArrayList<>();
        int i = 0;
        int j = 0;
        int k = 0;

        while (i < a.size() - 1 || j < b.size() - 1)
            if (a.get(i) < b.get(j))
                merged.add(k++, a.get(i++));
            else
                merged.add(k++, b.get(j++));

        return merged;
    }
}
