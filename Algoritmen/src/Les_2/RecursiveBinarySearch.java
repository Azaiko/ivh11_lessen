package Les_2;

/**
 * Created by Rodi on 16-2-2017.
 */
public class RecursiveBinarySearch {

    public static void recursiveBinarySearch(int answer, int lowerbound, int upperbound, int[] intList, int comparison){
        int position;
        position = (lowerbound + upperbound) / 2;
        comparison++;

        if ((intList[position] != answer) && (lowerbound <= upperbound)){
            if (intList[position] > answer) {
                upperbound = position - 1;
            } else {
                lowerbound = position + 1;
            }
            position = (lowerbound + upperbound) / 2;
            recursiveBinarySearch(answer, lowerbound, upperbound, intList, comparison);
        } else if (lowerbound <= upperbound) {
            System.out.println("The number that was found is: " + position + "\nIt was found after " + comparison + " comparisons.");
        } else {
            System.out.println("Sorry, the number is not in this array. \nWe made " + comparison + " comparisons.");
        }
    }
}
