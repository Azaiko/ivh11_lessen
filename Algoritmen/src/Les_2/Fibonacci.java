package Les_2;

/**
 * Created by Rodi on 13-Feb-17.
 */
public class Fibonacci {

    public static int fibo(int n, int m){
        if (n < 5000) {
            int newNumber = n + m;
            n = m;
            m = newNumber;
            System.out.println(newNumber);
            return fibo(n, m);
        } else
            return 0;
    }

    public static void noRecursion(){
        int n = 1;
        int m = 1;
        for (int i = 0; i < 20; i++){
            int newNumber = n + m;
            n = m;
            m = newNumber;
            System.out.println(newNumber);
        }
    }
}
