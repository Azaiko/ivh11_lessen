package Les_2;

/**
 * Created by Rodi on 13-Feb-17.
 */
public class Palindroom {

    public static Boolean checkPalindroom(String p){
        if(p.length() == 0 || p.length() == 1)
            return true;
        if(p.charAt(0) == p.charAt(p.length()-1))
            return checkPalindroom(p.substring(1, p.length()-1));
        return false;
    }
}
