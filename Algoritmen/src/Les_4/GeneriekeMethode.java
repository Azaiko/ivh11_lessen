package Les_4;

/**
 * Created by Rodi on 07-Mar-17.
 */
public class GeneriekeMethode {

    public static <E extends Number> double maxRowSum(E[][] array){

        double rowValue = 0;
        double maxRowValue = 0;
        int index = 0;

        for (int i = 0; i < array.length; i++) {
            E[] sub = array[i];
            for (int x = 0; x < sub.length; x++) {
                System.out.print(sub[x] + " ");
            }
            System.out.println();
        }

        System.out.println();

        for (int i = 0; i < array.length; i++) {
            E[] sub = array[i];
            for (int x = 0; x < sub.length; x++) {
                rowValue += sub[x].doubleValue();
                System.out.print(rowValue + " ");
            }
            if (rowValue > maxRowValue){
                maxRowValue = rowValue;
                index = i;
            }
            rowValue = 0;
            System.out.println();
        }

        System.out.println();
        System.out.println(maxRowValue);
        System.out.println(index);

        return maxRowValue;
    }
}
