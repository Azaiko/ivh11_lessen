package Stamboom;

/**
 * Created by Rodi on 15-Mar-17.
 */
public class Person {
    private String name;
    private String dateOfBirth;
    private String dateOfDeath;
    private String sex;

    public Person(String name, String dateOfBirth, String dateOfDeath, String sex) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.dateOfDeath = dateOfDeath;
        this.sex = sex;
    }

    public String toString(){
        return "\nNaam: " + name + "\nGeboortedatum: " + dateOfBirth + "\nOverlijden: " + dateOfDeath + "\nSex: " + sex;
    }

    public boolean isDeath(){
        if(dateOfDeath.matches(""))
            return false;
        else
            return true;
    }
}
