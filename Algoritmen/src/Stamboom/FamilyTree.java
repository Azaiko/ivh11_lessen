package Stamboom;

import java.util.List;

/**
 * Created by Rodi on 15-Mar-17.
 */
public class FamilyTree {

    private Node root;

    public FamilyTree(){
        root = new Node<>(new Person("Rodi", "29-01-1995","","man"));
        Node n2 = new Node<>(new Person("Adri", "11-04-1957","","man"));
        Node n3 = new Node<>(new Person("Sveta", "18-02-1967","","vrouw"));
        Node n4 = new Node<>(new Person("Heleen", "05-05-1932","","vrouw"));
        Node n5 = new Node<>(new Person("Dirk", "07-10-1931","26-06-2001","man"));
        Node n6 = new Node<>(new Person("Ivan", "03-07-1934","","man"));
        Node n7 = new Node<>(new Person("Maria", "12-11-1927","17-03-1983","vrouw"));

        root.addChild(n2);
        root.addChild(n3);

        n2.addChild(n4);
        n2.addChild(n5);

        n3.addChild(n6);
        n3.addChild(n7);
    }

    public Node<Person> getNodes(){
        return root;
    }

    public List<Node<Person>> noChildren(Node<Person> node){
        return null;
    }

    public Node<Person> recursiveFindParent(Node<Person> parent, Node<Person> child){
        // if the given parent contains the child, it returns the parent.
        if(parent.getChildren().contains(child)){
            return parent;
        }

        // if not it gets all children for each child of the given parent
        for(Node<Person> child2 : parent.getChildren()){

            // for that child it calls itself while passing the new parent and the same node
            Node<Person> p = recursiveFindParent(child2, child);

            // if p != null it returns P
            if(p != null){
                return p;
            }
        }

        // if no parent was found, we return a null value back
        return null;
    }

    public String getDeadChildren(){
        return null;
    }
}
