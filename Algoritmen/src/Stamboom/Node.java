package Stamboom;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Rodi on 15-Mar-17.
 */
public class Node<E> {
    private final E data;
    private final List<Node<E>> children;

    public Node(E data){
        this.data = data;
        children = new LinkedList<>();
    }

    public List<Node<E>> getChildren(){
        return children;
    }

    public E getData(){
        return data;
    }

    public void addChild(Node<E> child){
        children.add(child);
    }

    @Override
    public String toString(){
        return data.toString();
    }

}
