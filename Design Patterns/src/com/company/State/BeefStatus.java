package com.company.State;

/**
 * Created by Rodi on 15-2-2017.
 */
public interface BeefStatus {
    public void reportState(Beef beef);
}
