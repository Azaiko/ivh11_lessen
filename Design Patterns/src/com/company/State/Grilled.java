package com.company.State;

/**
 * Created by Rodi on 15-2-2017.
 */
public class Grilled implements BeefStatus{

    @Override
    public void reportState(Beef beef) {
        System.out.println("Beef is in the grilled state");
        beef.setState(this);
    }

    public String toString(){
        return "Stop state";
    }
}
