package com.company.State;

/**
 * Created by Rodi on 15-2-2017.
 */
public class StateMain {

    public static void main(String[] args) {
        Beef beef = new Beef();

        Raw raw = new Raw();
        raw.reportState(beef);

        beef.getBeefStatus();

        Grilled grilled = new Grilled();
        grilled.reportState(beef);

        beef.getBeefStatus();

        Burned burned = new Burned();
        burned.reportState(beef);

        beef.getBeefStatus();
    }
}

