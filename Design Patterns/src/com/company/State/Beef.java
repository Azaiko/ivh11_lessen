package com.company.State;

/**
 * Created by Rodi on 15-2-2017.
 */
public class Beef {
    private BeefStatus beefStatus;

    public Beef(){
        beefStatus = null;
    }

    public void setState(BeefStatus beefStatus){
        this.beefStatus = beefStatus;
    }

    public BeefStatus getBeefStatus(){
        return beefStatus;
    }
}
