package com.company.Strategy;

/**
 * Created by Rodi on 15-2-2017.
 */
public interface Behaviour {
    public void getBehaviour();
}
