package com.company.Strategy;

/**
 * Created by Rodi on 15-2-2017.
 */
public class NPC {
    private Behaviour behaviour;

    public NPC (Behaviour behaviour){
        this.behaviour = behaviour;
    }

    public void getBehaviour(){
        behaviour.getBehaviour();
    }
}
