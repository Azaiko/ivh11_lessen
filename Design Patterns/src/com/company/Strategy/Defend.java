package com.company.Strategy;

/**
 * Created by Rodi on 15-2-2017.
 */
public class Defend implements Behaviour {
    @Override
    public void getBehaviour() {
        System.out.println("Behaviour set to Defend");
    }
}
