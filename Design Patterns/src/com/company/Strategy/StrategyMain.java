package com.company.Strategy;

/**
 * Created by Rodi on 15-2-2017.
 */
public class StrategyMain {

    public static void main(String[] args) {
        NPC npc = new NPC(new Idle());
        npc.getBehaviour();

        npc = new NPC(new Defend());
        npc.getBehaviour();

        npc = new NPC(new Attack());
        npc.getBehaviour();
    }
}

