package sample.web.ui.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Rodi on 09-Feb-17.
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@Component
public class Product {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private int price;

    public Product(String name, int price){
        this.name = name;
        this.price = price;
    }

    public Product(Product p){
        this.name = p.name;
        this.price = p.price;
    }
}
