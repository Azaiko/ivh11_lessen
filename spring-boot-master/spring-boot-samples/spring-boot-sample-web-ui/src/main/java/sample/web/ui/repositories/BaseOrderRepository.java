package sample.web.ui.repositories;

import org.springframework.data.repository.CrudRepository;
import sample.web.ui.interfaces.BaseOrder;

/**
 * Created by Rodi on 21-2-2017.
 */
public interface BaseOrderRepository extends CrudRepository<BaseOrder, Long> {

}
