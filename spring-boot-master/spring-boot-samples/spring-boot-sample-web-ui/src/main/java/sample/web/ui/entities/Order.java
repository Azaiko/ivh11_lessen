package sample.web.ui.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sample.web.ui.interfaces.BaseOrder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rodi on 09-Feb-17.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Order extends BaseOrder {

    @OneToMany(cascade = javax.persistence.CascadeType.ALL)
    private List<Product> products = new ArrayList<>();

    public void add(Product product){
        products.add(product);
    }

    @Override
    public int price(){
        int price = 0;
        for(Product item : products){
            price += item.getPrice();
        }
        return price;
    }
}