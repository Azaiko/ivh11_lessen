package sample.web.ui.repositories;

import org.springframework.data.repository.CrudRepository;
import sample.web.ui.entities.Order;

/**
 * Created by Rodi on 09-Feb-17.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {
}
