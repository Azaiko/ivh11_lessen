package sample.web.ui.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sample.web.ui.interfaces.BaseOrder;
import sample.web.ui.interfaces.DecoratedOrder;

import javax.persistence.Entity;

/**
 * Created by Rodi on 21-2-2017.
 */
@Entity
@Getter
@Setter
public class OrderOption extends DecoratedOrder {

    private String name;
    private int price;

    public OrderOption(String name, int price, BaseOrder order){
        super(order);
        this.name = name;
        this.price = price;
    }

    @Override
    public int price(){
        return price + order.price();
    }

    public String toString(){
        return "option: " + name;
    }

}
