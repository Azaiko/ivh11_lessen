package sample.web.ui.interfaces;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by Rodi on 21-2-2017.
 */

@Entity
public abstract class DecoratedOrder extends BaseOrder{

    @ManyToOne(cascade = javax.persistence.CascadeType.ALL)
    protected BaseOrder order;

    protected DecoratedOrder(BaseOrder baseOrder){
        this.order = baseOrder;
    }
}
