package sample.web.ui.repositories;

import org.springframework.data.repository.CrudRepository;
import sample.web.ui.entities.ProductCatalog;

/**
 * Created by Rodi on 09-Feb-17.
 */
public interface ProductCatalogRepository extends CrudRepository<ProductCatalog, Long> {
}
