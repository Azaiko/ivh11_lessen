package sample.web.ui.interfaces;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Rodi on 21-2-2017.
 */
@Entity
public abstract class BaseOrder {

    @Id
    @GeneratedValue
    private long id;

    public abstract int price();
}
